1. Return the customerName of the customers who are from the Philippines.

Answer: SELECT customerName FROM customers WHERE country = "Philippines";

2.Return the customerName of the customers who are from the US.

Answer: SELECT customerName FROM customers WHERE country = "USA";

3. Return the contactLastName and contactFirstName of the customers with name "La Rochelle gifts".

Answer: SELECT contactLastName,contactFirstName FROM customers WHERE customerName = "La Rochelle gifts";

4. Return the product name and MSRP of the product named "The Titanic".

Answer: SELECT productName,MSRP FROM products WHERE productName = "The Titanic";

5. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com".

Answer: SELECT firstName,lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

6. Return the names of customers who have no registered state.

Answer: SELECT customerName, state FROM customers WHERE state IS NULL;

7. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve.

Answer: SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

8. Return customer name, country and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000.

Answer: SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

9. Return the customer numbers of orders whose comments contain the string 'DHL'.

Answer: SELECT customerNumber,orderNumber FROM orders WHERE comments LIKE %DHL%;

10. Return product lines whose text description mentions the phrase 'state of the art'.

Answer: SELECT productline FROM productlines WHERE textDescription Like "%state of the art%";

11. Return the countries of customers without duplication.

Answer: SELECT DISTINCT country FROM customers;

12. Return the statuses of orders without duplication.

Answer: Select DISTINCT status FROM orders;

13. Return the customer names and countries whose country is USA, France, or Canada.

Answer: SELECT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR "Canada";

14. Join offices and employees and return the first name, last name, and office's city of employees where their office is in Tokyo.

Answer: SELECT employees.firstName, employees.lastName, offices.city  FROM offices
        JOIN employees ON offices.officeCode = employees.officeCode
        WHERE offices.city = "Tokyo";

15. Join customers and employees and return the names of customers where the employee who served them is Leslie Thompson.

Answer: SELECT customers.customerName FROM employees 
        JOIN customers ON employeeNumber = customers.salesRepEmployeeNumber
        WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";

16. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000.

Answer: SELECT productName, quantityInStock FROM products WHERE  productLine LIKE "%planes%" AND quantityInStock < 1000;

17. Return customer's name with a phone number containing "+81".

Answer: SELECT customerName FROM customers WHERE phone LIKE "+81%";

18. Return the number of customers in the UK.

Answer: SELECT COUNT(customerNumber) FROM customers WHERE country ="UK";